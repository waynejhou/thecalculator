﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TheCalculator
{
    public enum InputSymbols {
        Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Dot,
        Positive, Negative, Product, Division, Pow,
        Clear, Pi, Sqrt,
        Result,
        None = -1
    }

    public static class EnumExtendMethods
    {
        public static ResourceDictionary InputSymbolsResourceStrings { get; set; } = null;
        public static string GetString(this InputSymbols symbols)
        {
            if (InputSymbolsResourceStrings != null)
                return (string)InputSymbolsResourceStrings[symbols.ToString()];
            return "";
        }

    }

}
