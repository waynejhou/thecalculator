﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCalculator
{
    class Calculator
    {
        public double? NumA { get; set; } = null;
        string _numAStr = "";
        public InputSymbols Operator { get; set; } = InputSymbols.None;
        public double? NumB { get; set; } = null;
        string _numBStr = "";
        public double? Result { get; set; } = null;

        public void Input(InputSymbols symbol, int position)
        {
            if (symbol == InputSymbols.Clear)
                Reset();
            if (symbol == InputSymbols.Result)
            {
                if (Result != null)
                {
                    NumA = Result.Value;
                    Result = null;
                }
                if (TryGetResult(out double result))
                {
                    Result = result;
                }
            }
            if ((int)symbol >= 0 && (int)symbol <= 10)
            {
                string tempString = "";
                if (Result != null)
                {
                    NumA = Result.Value;
                    Result = null;
                    NumB = null;
                    _numBStr = "";
                }
                if (symbol == InputSymbols.Dot)
                {
                    if (Operator == InputSymbols.None)
                        tempString = _numAStr + ".";
                    else
                        tempString = _numBStr + ".";
                }
                else
                {
                    
                    if (Operator == InputSymbols.None)
                        tempString = _numAStr + ((int)symbol).ToString();
                    else
                        tempString = _numBStr + ((int)symbol).ToString();
                }
                if (double.TryParse(tempString, out double result))
                {
                    if (Operator == InputSymbols.None)
                    {
                        _numAStr = tempString;
                        NumA = result;
                    }
                    else
                    {
                        _numBStr = tempString;
                        NumB = result;
                    }

                }
            }
            if((int)symbol >= 11 && (int)symbol <= 15)
            {
                if (Result != null)
                {
                    NumA = Result.Value;
                    Result = null;
                    NumB = null;
                    _numBStr = "";
                }
                else
                {
                    if (NumB != null)
                    {
                        if (TryGetResult(out double result))
                        {
                            Result = result;
                        }
                        NumA = Result.Value;
                        Result = null;
                        NumB = null;
                        _numBStr = "";
                        Operator = symbol;
                    }
                    if (NumA != null)
                        Operator = symbol;
                }

            }
        }

        bool TryGetResult(out double result)
        {
            if (NumA == null || NumB == null || Operator == InputSymbols.None)
            {
                result = double.NaN;
                return false;
            }
            switch (Operator)
            {
                case InputSymbols.Positive:
                    result = NumA.Value + NumB.Value;
                    break;
                case InputSymbols.Negative:
                    result = NumA.Value - NumB.Value;
                    break;
                case InputSymbols.Product:
                    result = NumA.Value * NumB.Value;
                    break;
                case InputSymbols.Division:
                    if (NumB.Value == 0)
                        result = double.NaN;
                    else
                        result = NumA.Value / NumB.Value;
                    break;
                case InputSymbols.Pow:
                    result = Math.Pow(NumA.Value, NumB.Value);
                    break;
                default:
                    result = double.NaN;
                    break;
            }
            return true;
        }

        void Reset()
        {
            NumA = null;
            _numAStr = "";
            Operator = InputSymbols.None;
            NumB = null;
            _numBStr = "";
            Result = null;
        }

        public override string ToString()
        {
            var str = "";
            if (NumA != null)
                str += $"{NumA} ";
            if (Operator != InputSymbols.None)
                str += $"{Operator.GetString()} ";
            if (NumB != null)
                str += $"{NumB} ";
            if (Result != null)
                str += $" = {Result}";
            return str;
        }
    }
}
